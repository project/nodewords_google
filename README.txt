Nodewords Google
----------------------------------------

This module allows you to set THE Google news_keywords meta tag.

This version of the module only works with Drupal 6.x. and have dependency
on "Nodewords: The Drupal 6 Meta Tags module"


Features
------------------------------------------------------------------------------
The primary features include:

* The current supported Google meta tag news_keywords.

* Meta tags can be assigned site-wide defaults and then overridden on a
  per-node, per-tag and per-path basis.

* It is possible to control which of the available tags will be available for
  editing versus only using the previously configured values.

* All text of the DESCRIPTION and KEYWORDS meta tags are added to the search
  system so they are searchable too; other meta tags could be added to the
  search system too (depending on the code implemented from the module).


Installing Nodewords Google (first time installation)
------------------------------------------------------------------------------
 1. First you need to install/enable "Nodewords: The Drupal 6 Meta Tags module".

 2. Backup your database.

 3. Copy the module as normal.
   More information about installing contributed modules could be found at
   "Install contributed modules" [4].

 4. Enable the "Nodewords Google meta tags" module from the module 
  administration page (Administer >> Site configuration >> Modules).

 5. Configure the module (see "README.txt" file in Nodewords module).


Known Issues
------------------------------------------------------------------------------
No


Related modules
------------------------------------------------------------------------------
Nodewords.


Credits / Contact
------------------------------------------------------------------------------
The current maintainer is Balraj Brar.

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  http://drupal.org/project/issues/nodewords_google


References
------------------------------------------------------------------------------
[1] http://drupal.org/project/nodewords
[2] http://googlenewsblog.blogspot.co.uk/2012/09/a-newly-hatched-way-to-tag-your
-news.html
